#include <SPI.h>

#define analogReadPin A0
#define directionPin 4
#define chipSelectPin 10
#define startByte 0xEC
#define address 28
#define stopByte 0x99

uint16_t currentRaw;
bool heard = false;

void setup() {
  pinMode(0, INPUT_PULLUP);
  pinMode(1, INPUT_PULLUP);
  pinMode(13, OUTPUT);
  // Bus Tranceiver
  Serial.begin(38400);
  //Direction pin
  pinMode(directionPin, OUTPUT);
  digitalWrite(directionPin, HIGH);

  //analog
  pinMode(analogReadPin, INPUT);

  
  //SPI.begin();
  pinMode(chipSelectPin, OUTPUT);
}

void loop() {
  readCurrent();
  if (read485()) {
    write485();
    heard = 0;
  }
  delay(5);
  digitalWrite(13, !digitalRead(13));
}
void write485() {
  digitalWrite(directionPin, LOW);
  delayMicroseconds(500);
  Serial.write(0xEC);
  Serial.write(0x1C);
  Serial.write((uint8_t)(currentRaw&0xFF00)>>8);
  Serial.write((uint8_t)(currentRaw&0x00FF)>>0);
  Serial.write(0x01);
  Serial.write(0x99);
  Serial.write(0x99);
  delay(5);
  digitalWrite(directionPin, HIGH);
}
bool read485() {
  if (Serial.available()) {
    while (Serial.available()) {
      addByte(Serial.read());
    }
  }
  return (heard == true);
}

void addByte(uint8_t receiveByte)
{  
  static bool callByte = false, me = false, stop1 = false;
  
  if (!callByte) { //if you havent heard a start byte yet
    if (receiveByte == startByte) //and the incomming is the start byte
    {      
      callByte = true; //set flag
      return; //bounce
    } else return; //if its not start byte and we havent heard one ignor it

  } else //else we have had our start byte
  {
   
    if (!me) //if we havent heard if its us after start byte
    {
      if (receiveByte == address){ // if it is us       
      me = true; //set flag
      return;
    }
    else //else
    {
      callByte = false;
      return;
    }
  } else {
    if ((receiveByte == stopByte) && stop1) { //if this is second stop byte in a row terminate packet read
      heard = true;
      callByte = false;
      stop1 = false;
      me = false;
      return;
    } else if (receiveByte == stopByte) //else if this is a stopByte w/o heard one yet
    {      
      stop1 = true;
      return;
    }
  }
}
}
void readCurrent() {
  currentRaw = analogRead(analogReadPin);
  
}
void setGain(uint8_t setting){
  digitalWrite(chipSelectPin,LOW);
  SPI.transfer(0x40);
   switch (setting){
     case 1:
       SPI.transfer(0);
     break;
     case 2:     
       SPI.transfer(1);
     break;
     case 4:     
       SPI.transfer(2);
     break;
     case 5:     
       SPI.transfer(3);
     break;
     case 8:     
       SPI.transfer(4);
     break;
     case 10:     
       SPI.transfer(5);
     break;
     case 16:     
       SPI.transfer(6);
     break;
     case 32:     
       SPI.transfer(7);
     break; 
    digitalWrite(chipSelectPin, HIGH);
   } 
}



