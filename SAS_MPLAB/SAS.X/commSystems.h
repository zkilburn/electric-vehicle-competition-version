/* 
 * File:   commSystems.h
 * Author: User
 *
 * Created on April 8, 2014, 6:17 AM
 */

#ifndef COMMSYSTEMS_H
#define	COMMSYSTEMS_H

#ifdef	__cplusplus
extern "C" {
#endif

    enum slaveSend {
        startB = 0, addr = 1, stopB1 = 2, stopB2 = 3, MAXENUMS = 4
    } sendPoint;

    struct slaveOut {
        unsigned char startB;
        char *addr;
        unsigned char stopB1;
        unsigned char stopB2;
    } SlaveOut;

    void setupCommData() {

        //bare ECU minimum packet contents (header)(says boot not complete

        ECUOut[0] = startByte;
        ECUOut[1] = address;
        ECUOut[2] = stopByte;
        ECUOut[3] = stopByte;
        ECUpacketSize = 4;

    }

    void writeCommTB() {
        for (i = 0; i < ECUpacketSize; i++) {
            if (i == 2) {
                putByteUART2(0x01);
                writeSensor(throttle1);
                writeSensor(throttle2);
                writeSensor(brake1);
                writeSensor(brake2);
            }
            putByteUART2(ECUOut[i]);
        }

    }

    void writeCommDataPacket() {
        for (i = 0; i < ECUpacketSize; i++) {
            if (i == 2) {
                putByteUART2(0x03);
                DELAY_100uS;
                writeSensor(throttle1);
                writeSensor(throttle2);
                writeSensor(brake1);
                writeSensor(brake2);
                writeSensor(speed1);
                writeSensor(speed2);
                writeSensor(speed3);
                writeSensor(speed4);
            }
            DELAY_100uS;
            putByteUART2(ECUOut[i]);
            DELAY_100uS;
        }
    }


    void writeSensor(int toSend) {

        unsigned char lowb = (toSend & 0x00FF);
        unsigned char highb = ((toSend >> 8) & 0x00FF);

        putByteUART2(highb);
        putByteUART2(lowb);
    }

    void putrsUART2(const unsigned char *data) {
        do {
            while (!IFS1bits.U2TXIF);
            IFS1bits.U2TXIF = 0;
            U2TXREG = *data;
        } while (*data++);
    }

    void putByteUART2(unsigned char data) {
        while (U2STAbits.UTXBF);
        U2TXREG = data;
    }

    void putIntUSB(int toSend) {
        unsigned char lowb = (toSend & 0x00FF);
        unsigned char highb = ((toSend >> 8) & 0x00FF);
        putByteUSB(highb);
        DELAY_100uS;
        putByteUSB(lowb);
        DELAY_100uS;

    }

    void putByteUSB(unsigned char data) {
        while (!IFS0bits.U1TXIF);
        U1TXREG = data;
    }

    void putStrUSB(char * data) {
        static int ptr = 0;
        while (data[ptr] != 0) {
            while (!IFS0bits.U1TXIF);
            putByteUSB(data[ptr]);
        }
    }

    void toggleECUdirection() {
        L_T = !L_T;
    }

#ifdef	__cplusplus
}
#endif

#endif	/* COMMSYSTEMS_H */

