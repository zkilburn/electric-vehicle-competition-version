/* 
 * File:   methods.h
 * Author: User
 *
 * Created on April 6, 2014, 4:46 PM
 */

#ifndef METHODS_H
#define	METHODS_H

#ifdef	__cplusplus
extern "C" {
#endif
//function declarations
void initRS485Direction();
void initInterrupts();
void initPins();
void initUARTS();
void setupCommData();
void checkSlaveComms();
void writeSlaveComms();
void toggleLED();
void delay_5ms();
void delay_1s();
void oscillatorInit();
void putrsUART2(const unsigned char *data);
void putByteUSB(unsigned char data);
void putByteUART2(unsigned char data);
void putIntUSB(int toSend);
void listenECU();
void toggleECUdirection();
void writeSensor(int toSend);
void writeCommDataPacket();
void writeCommTB();
void readADCValues();
void brakeLightCheck();

#ifdef	__cplusplus
}
#endif
#endif	/* METHODS_H */