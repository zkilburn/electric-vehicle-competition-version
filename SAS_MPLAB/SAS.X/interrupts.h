/* 
 * File:   interrupts.h
 * Author: User
 *
 * Created on April 6, 2014, 4:46 PM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C" {
#endif

void initInterrupts() {
        INTCON1bits.NSTDIS = 1; //no nesting of interrupts
        IFS0bits.U1TXIF = 0;
        IFS1bits.U2TXIF = 0;
        IFS0bits.U1RXIF = 0;
        IFS1bits.U2RXIF = 0;
    }
int buffer[20];
int ADC;
int ADCdata=0;
int ports[6]={0,1,4,5,6,7};
 void __attribute__((interrupt, no_auto_psv)) _ADC1Interrupt(void) {
     if(ADCdata==0){     
     buffer[ADC]=ADC1BUF0;
     ADC++;            
    }
         if(ADC>5){
             ADCdata=1;
             ADC=0;

        // IEC0bits.AD1IE=0;
     }
     AD1CHS0bits.CH0SA=ports[ADC];
     AD1CON1bits.DONE=0;
     AD1CON1bits.SAMP = 1; 
     IFS0bits.AD1IF=0;
 }
    void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void) {
//        if (ECUbyteNum < ECUpacketSize) {
//            DELAY_100uS;
//            U1TXREG = ECUOut[ECUbyteNum];
//            ECUbyteNum++;
//        } else {
//            responseSent = 1;
//            ECUbyteNum = 1;
//            L_T1 = LISTEN;
//        }
        //delay_5ms();
        //U1TXREG = ECUOut[0];
        IFS0bits.U1TXIF = 0;
    }

//    void __attribute__((interrupt, no_auto_psv)) _U2TXInterrupt(void) {
////    if (slaveByteNum < slavePacketSize) {
////        U2TXREG = SlaveOut[slaveByteNum];
////        slaveByteNum++;
////    } else {
////        slaveByteNum = 1;
////        slaveSent = 1;
////        L_T2=!LISTEN;
////    }
//        slaveSent=1;
//        //putrsUART2("Test");
//        IFS1bits.U2TXIF = 0;
//    }
    //For when the ECU comes knocking



    void addByte(unsigned char incomming){
        if(!startHeard){         //if you havent heard a start byte yet
            if(incomming==startByte)         //and the incomming is the start byte
            {
                startHeard=1;        //set flag
                return;             //bounce
            }
            else return;        //if its not start byte and we havent heard one ignor it

        }
        else        //else we have had our start byte
        {
            if(!me)                //if we havent heard if its us after start byte
            {
                if(incomming==address){   // if it is us
                    me=1;
                    return;//set flag
                }
                else                    //else
                {
                    startHeard=0;
                    return;
                }
            }
            else
            {                
                if (incomming==stopByte && stop1){  //if this is second stop byte in a row terminate packet read                    
                    packetReady=1;
                    startHeard=0;
                    stop1=0;
                    me=0;
                    return;
                }
                else if (incomming==stopByte)       //else if this is a stopByte w/o heard one yet
                {
                    stop1=1;
                    return;
                }
                else {
                    inputCmd=incomming;
                }
                
            }

        }

    }
    void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void) {        
        
        IFS0bits.U1RXIF = 0;
    }
    //For when a slave knocks

    void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void) {

        while(U2STAbits.URXDA==1){
        addByte(U2RXREG);
        }
        IFS1bits.U2RXIF = 0;
    }

    void __attribute__((interrupt, auto_psv)) _INT0Interrupt(void) {
        IFS0bits.INT0IF = 0;
    }

#ifdef	__cplusplus
}
#endif
#endif
