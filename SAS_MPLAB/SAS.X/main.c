
/* SAS
 * File:   main.c
 * Author: pwnedmark (revised by bozzobrain)
 *
 * Created on March 29, 2014, 4:35 AM (Reborn on April 3rd, 2014 1:31 AM)
 *
 */

#include <stdlib.h>
#include <xc.h>
#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "definitions.h"
#include "methods.h"
#include "variables.h"
#include "initSystems.h"

#include "commSystems.h"
#include "interrupts.h"

/*
 *
 */
int readings[20];
static int ADCT=0;
#define ADCTime 10000



int main(int argc, char** argv) {
    initPins();
    oscillatorInit();
    initInterrupts();
    setupCommData();
    initUARTS();
    initADC();
    initRS485Direction();
    delay_5ms();

    while (1) {        
        readADCValues();
        brakeLightCheck();
        listenECU();
        toggleLED();
    }
    return (EXIT_SUCCESS);
}

void listenECU() {
    if (packetReady) {
        IEC0bits.AD1IE = 1;
        toggleECUdirection();
        delay_5ms();
        switch (inputCmd) {
            case 1:
                writeCommTB();
                break;
            case 3:
                writeCommDataPacket();
                break;
        }
        delay_5ms();
        delay_5ms();
        delay_5ms();
        inputCmd = 0;
        packetReady = 0;
        toggleECUdirection();
    } 
}

void toggleLED() {
    if (LEDTime > LEDT) {
        LED = !LED;
        LEDTime = 0;
    } else LEDTime++;
}

void readADCValues() {
    if ((ADCdata == 1)) {
        for (i = 0; i < 6; i++) {
            DELAY_100uS;
            readings[i] = buffer[i];
        }
        ADCdata=0;
        //IEC0bits.AD1IE=1;
        if(readings[1]>450){
            readings[1]=450;

        }
        throttle1 = ((450-readings[1])/2)*40.95;
        throttle2 = readings[2];
        brake1 = (readings[3]-981);
        brake2 = readings[4];
        ADCT=0;
    } else if (ADCdata==1){
        ADCT++;
    }
}

void brakeLightCheck(){
    if ((brake1)>6){
        BrakeLight=1;
    }else
        BrakeLight=0;
}
