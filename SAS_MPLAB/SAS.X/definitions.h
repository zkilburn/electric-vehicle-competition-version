/* 
 * File:   definitions.h
 * Author: User
 *
 * Created on April 6, 2014, 4:45 PM
 */

#ifndef DEFINITIONS_H
#define	DEFINITIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define startByte 0xEC
#define stopByte 0x99
#define address 0x01

//asm delay function -- should work :)
#define DELAY_100uS asm volatile ("REPEAT, #2500"); Nop();
//some constant declarations
#define numSlaves 28
#define LEDT 50 //200 counts of 5ms till LED toggle (~1 second)
#define ST 8 //50 counts of 5ms for slave retransmit (~250 ms)

//Baud rate generator help
#define FP 12500000
#define BAUDRATE384 38400
#define BAUDRATE96 9600
#define BRGVAL ((FP/BAUDRATE384)/16)-1
#define BRGVAL2 ((FP/BAUDRATE96)/16)-1

//READABILITY DEFINES
#define INPUT 1
#define OUTPUT 0
#define HIGH 1
#define LOW 0
#define TALK 1
#define LISTEN 0
//OUTPUT COMMAND
#define LEDTRIS TRISBbits.TRISB12
#define L_TTRIS TRISAbits.TRISA9
#define LED LATBbits.LATB12 //SIGN OF LIFE LED
#define L_T LATAbits.LATA9 //RS485 DIRECTION

#define AN0 TRISAbits.TRISA0
#define AN1 TRISAbits.TRISA1
#define AN2 TRISCbits.TRISC0
#define AN3 TRISBbits.TRISB2
#define AN4 TRISBbits.TRISB3
#define AN5 TRISCbits.TRISC1

#define AN0L LATAbits.LATA0
#define AN1L LATAbits.LATA1
#define AN2L LATCbits.LATC0
#define AN3L LATBbits.LATB2
#define AN4L LATBbits.LATB3
#define AN5L LATCbits.LATC1

#define RxPullup1 LATCbits.LATC3
#define TxPullup1 LATCbits.LATC4
#define RxPullup2 LATCbits.LATC6
#define TxPullup2 LATCbits.LATC7
//UART Definitions
#define U1Rx_RPn RPINR18bits.U1RXR
#define U2Rx_RPn RPINR19bits.U2RXR
#define RP12map RPOR6bits.RP12R
#define RP19map RPOR9bits.RP19R
#define RP20map RPOR10bits.RP20R
#define RP22map RPOR11bits.RP22R
#define RP23map RPOR11bits.RP23R
#define U1Tx_RPn 3
#define U2Tx_RPn 5

#define BrakeLight LATAbits.LATA4

//INTERRUPT0 pin config
#define Interrupt0Pin RPINR0bits.INT1R



#endif	/* DEFINITIONS_H */

