

void initializeCommSystems();
void sendDataMCS();
bool readSAS();
void requestBMSupdate();
void setupPins();
bool variableIsAbout(int var1, int var2, int range);
void driveStart();
void blowHorn(int duration);
void toggleLED();
void buttonTEST();
int readSS();
void request_SAS_All();
void throttleUpdateCheck();
void requestBatteryInformations();
void requestFaultUpdate();
bool readMCS();
bool responseMCS();
void sendStopMCS();

void toggleLED() {
  static int SSin = 0;
  LEDTimer.updateTimer();
  LED2Timer.updateTimer();
  if (LED2Timer.timerDone()) {
    if (!readyToDrive && startBoot) {
      if ((readSS() > 300)) {
        digitalWrite(BLED2, !digitalRead(BLED2));
        //Serial.println("SS firing");
      }
      else {
        if (SSin > 4) {
          digitalWrite(BLED2, !digitalRead(BLED2));
          // Serial.println("SS input missing");
          SSin = 0;
        } else
          SSin++;
      }
    }
    else {

    }

  }
  if (LEDTimer.timerDone()) {
    digitalWrite(13, !digitalRead(13));
    //digitalWrite(BLED1, !digitalRead(BLED1));
  }
}

void buttonTEST() {

  if (!digitalRead(BUTA)) {
    requestBatteryInformations();
  }
  else {
  }

  if (digitalRead(BUTB)) {
    readyToggle = true;
  }
  else {
    if (readyToggle) {
      digitalWrite(BLED2, !digitalRead(BLED2));
      toggleRelay = true;
      if (!startBoot && (!readyToDrive && ((brake2 > 0)))) {
        startBoot = true;
      }
      else {
        if (!readyToDrive)
          digitalWrite(BLED2, LOW);
        startBoot = false;
      }
      readyToggle = false;
    }
  }

}


bool variableIsAbout(int var1, int var2, int range) {
  return ((var1 < var2 + range) && (var1 < var2 - range));
}

void blowHorn(int duration) {
  digitalWrite(HORN, HIGH);
  delay(duration);
  digitalWrite(HORN, LOW);
}

void setupPins() {

  //EXT
  pinMode(EXT, INPUT);
  //RTG
  pinMode(RTG, INPUT);
  //SS
  pinMode(SS, OUTPUT);
  //RTD Horn
  pinMode(HORN, OUTPUT);
  //Button A
  pinMode(BUTA, INPUT);
  //Button B
  pinMode(BUTB, INPUT);
  //Button LED
  pinMode(BLED1, OUTPUT);
  pinMode(BLED2, OUTPUT);
  digitalWrite(BLED2, LOW);
  digitalWrite(SS, LOW);
  digitalWrite(RTG, HIGH);
  digitalWrite(EXT, HIGH);
}

void initializeCommSystems() {
  //turn on serial comm

  //repurposed 328 transciever - SAS/BMS
  Serial1.begin(38400);
  pinMode(TLBMSSAS, OUTPUT);
  digitalWrite( TLBMSSAS, HIGH);


  //Main transciever - MCS
  Serial2.begin(38400);
  pinMode(TLMCS, OUTPUT);
  digitalWrite(TLMCS, HIGH);


  //DEBUG USB
  Serial.begin(38400);
}

void faultSystem() {
  digitalWrite(SS, LOW);
  digitalWrite(BLED2, LOW);
  readyToDrive = false;
  MCSstable = false;
  BMSready = false;
  throttle1 = 0;
  throttle2 = 0;
  brake1 = 0;
  brake2 = 0;
  sendDataMCS();
  //sendStopMCS();
  driveStart();
}
bool flagTBCompare = false;
bool throttleBrakeCompare() {

  if (flagTBCompare) {
    if ((throttle1 < 15) && (brake2 < 25)) {
      flagTBCompare = false;
      Serial.println("TB fault clear");
    }
  }
  else if (throttle1 > 25) {
    if (brake2 > 25) {
      flagTBCompare = true;
      Serial.println("TB fault set");
    }
  }

  if (flagTBCompare)
    return true;
  else
    return false;


}


bool throttleDataCheck() {
  
  
    static Timers speedTimer(1000);
  speedTimer.updateTimer();
  if (speedTimer.timerDone()) {
    //requestBatteryInformations();
    //   requestFaultUpdate();
    //
    //    requestBatteryInformations();
    //  readMCS();
    //  Serial.print("Current Usage:  ");
    //  Serial.println(current);
    //  Serial.println(" ");
//  Serial.print("Throttle 1: ");
//  Serial.println(throttle1);
//  Serial.print("Throttle 2: ");
//  Serial.println(throttle2);
//  Serial.print("Brake 1   : ");
//  Serial.println(brake1);
//  Serial.print("Brake 2   : ");
//  Serial.println(brake2);
    if ((speedFrontLeft > 0) || (speedFrontRight > 0) || (speedRearLeft > 0) || (speedRearRight > 0)) {
      Serial.println(" ");
      Serial.print("Wheel speed front left:  ");
      Serial.println((speedFrontLeft * 0.4351));
      Serial.print("Wheel Speed front right:  ");
      Serial.println((speedFrontRight * 0.4351));
      Serial.print("Wheel Speed Rear left:  ");
      Serial.println((speedRearLeft * 0.85227));
      Serial.print("Wheel Speed Rear Right:  ");
      Serial.println((speedRearRight * 0.85227)); //     *3600/5280
      speedRearRight = 0;
      speedRearLeft = 0;
      speedFrontRight = 0;
      speedFrontLeft = 0;
    }
  }
  
  
  if(throttleBrakeCompare()){
    return false;
  }
  if (throttle2 < 11) {
    if ((throttle2 < throttle1 + 10) && (throttle1 < throttle2 + 10))  {
      throttle1 = throttle1 * 40.95;
      throttle2 = throttle2 * 40.95;
     
      return true;
    }
    else
    return false;
  } else
  {
    if ((throttle1 > throttle2 - 10) && (throttle1 < throttle2 + 10))  {
      throttle1 = throttle1 * 40.95;
      throttle2 = throttle2 * 40.95;
      return true;
    }
    else
     return false;
  }


return false;

}



