bool MCSstable=false;
bool BMSready=false;
static bool startBoot=false;
uint16_t brakeOut, throttleOut;
Timers LEDTimer(1000);
Timers LED2Timer(250);

bool faultFlag = false;
bool batteryFlag = false;
bool faultLock = false;
int inByte = 0;         // incoming serial byte
uint16_t throttle1 = 0, throttle2 = 0;
uint16_t brake1 = 0, brake2 = 0;
uint16_t speedFrontLeft = 0, speedFrontRight = 0;
uint16_t speedRearLeft = 0, speedRearRight = 0;
uint8_t batteryVolt[28],batteryTemp[28];
bool SASRequestFinish = false;
bool readyToDrive = false;
uint16_t SSread;
int unheard = 0;
int notListening = 0;
static bool readyToggle=false,toggleRelay=false;

union {
  uint16_t result;
  struct {
    uint8_t low;
    uint8_t high;
  }
  join;
}
joint;

uint16_t current;
uint16_t temp1, temp2;
