//Talk listen pin (direction 485) settings
#define TLBMSSAS 42     //3
#define TLMCS 6 //27
#define HORN 22
#define BUTA 2
#define BUTB 3
#define BLED1 4
#define BLED2 5
#define EXT 25
#define RTG 24
#define SS 23
#define SStest A0



#define ECUADDRESS 0x00

#define MCSADDRESS 0x02
#define MCSUPDATE 0x01
#define MCSSTOP 0x02

#define SASADDRESS 0x01
#define SAS_REQUEST_WS  0x02
#define SAS_REQUEST_TB 0x01
#define SAS_REQUEST_ALL 0x03

#define BMSADDRESS 0x03
#define FAULTUPDATE 0x01
#define BATTERYUPDATE 0x02
#define numBatteries 28

#define startByte 0xEC
#define stopByte 0x99
