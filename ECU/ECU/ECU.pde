//COM Serial1 =mcs
//Serial3 = SAS

#define ECUADDRESS 0x00

#define MCSADDRESS 0x02
#define MCSUPDATE 0x01
#define MCSSTOP 0x02

#define SASADDRESS 0x01
#define SAS_REQUEST_WS  0x02
#define SAS_REQUEST_TB 0x01
#define SAS_REQUEST_ALL 0x03

#define BMSADDRESS 0x03
#define FAULTUPDATE 0x01
#define BATTERYUPDATE 0x02
#define numBatteries 28

#define startByte 0xEC
#define stopByte 0x99

bool faultFlag=false;
bool batteryFlag=false;
bool faultLock=false; 
int inByte = 0;         // incoming serial byte
uint16 throttle1=0, throttle2=0;
uint16 brake1=0, brake2=0;
uint16 speedFrontLeft=0, speedFrontRight=0;
uint16 speedRearLeft=0, speedRearRight=0;
uint16 batteryInfo[28];
bool SASRequestFinish=false;
bool readyToDrive=false;

void initializeCommSystems();
void sendDataMCS();
bool readSAS();
void requestBMSupdate();
void setupPins();
bool variableIsAbout(int var1, int var2, int range);
void driveStart();
void blowHorn(int duration);

void blowHorn(int duration){
   digitalWrite(11,HIGH);
      delay(duration);
      digitalWrite(11,LOW);
}

void setup() {
  setupPins();
  pinMode(BOARD_LED_PIN, OUTPUT);
  initializeCommSystems();  
  //driveStart();
}

void setupPins(){
  //RTG
  pinMode(7,OUTPUT);
  //SS
  pinMode(5,OUTPUT);
  //RTD Horn
  pinMode(11,OUTPUT);
  //Button A
  pinMode(17, INPUT);
  //Button B
  pinMode(18,INPUT);
  //Button LED
  pinMode(4,OUTPUT);
}

void initializeCommSystems(){
  //turn on serial comm
  Serial1.begin(38400);
  Serial2.begin(38400);
  Serial3.begin(38400);
  //set output to direction pins 485
  pinMode(3,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(27,OUTPUT);
  digitalWrite(3,HIGH);
  digitalWrite(10,HIGH);
  digitalWrite(27,HIGH);  
  //pullups on recieve lines
  digitalWrite(25,HIGH);
  digitalWrite(8,HIGH);
  digitalWrite(0,HIGH);
  //transmit lines
  digitalWrite(26,HIGH);
  digitalWrite(9,HIGH);
  digitalWrite(1,HIGH);

}

void driveStart(){
  while(!readyToDrive){
    if((digitalRead(17)==1)&&(digitalRead(6)==1)&& (brake1<=1000)){
      digitalWrite(5,HIGH);
      for(int i=0;i<6;i++)
        delay(1000);
      blowHorn(3000);
      readyToDrive=true;
      break;
    }
  }
}

void faultSystem(){
   digitalWrite(5,LOW);
    while(1){
      delay(500);
      toggleLED();
    }
}

void loop() {
  throttleUpdateCheck(); 
  if(throttleDataCheck()){
    sendDataMCS();  
  }
  else {
    //faultLock=true;
  }
  delay(3);
  readMCS();
  requestFaultUpdate();
  toggleLED();
  if (faultLock){    
    //faultSystem();
    }
}

bool throttleDataCheck(){
  Serial2.println();
  Serial2.print("Throttle 1: ");
  Serial2.println(throttle1);
  Serial2.print("Throttle 2: ");
  Serial2.println(throttle2);
  Serial2.print("Brake 1   : ");
  Serial2.println(brake1);
  Serial2.print("Brake 2   : ");
  Serial2.println(brake2);
  Serial2.println();
  return true;
}

int unheard=0;
int notListening=0;

void throttleUpdateCheck() {
  request_SAS_TB();
  delay(10);
  while(!readSAS()){
    delay(10);
    unheard++;
    if (unheard>10){
      unheard=0;
      notListening++;
      if (notListening>5){
        notListening=0;        
        faultLock=true; 
        break;
      }
      //request_SAS_All();
    } 
  }   
  notListening=0;
  unheard=0; 
}

void request_SAS_All(){
  digitalWrite(3,HIGH); 
  delayMicroseconds(2500);
  Serial3.write(startByte);
  Serial3.write(SASADDRESS);
  Serial3.write(SAS_REQUEST_ALL);
  Serial3.write(stopByte); 
  Serial3.write(stopByte);
  delayMicroseconds(500);
  digitalWrite(3,LOW);
}

void request_SAS_TB(){
  digitalWrite(3,HIGH); 
  delayMicroseconds(2500);
  Serial3.write(startByte);
  Serial3.write(SASADDRESS);
  Serial3.write(SAS_REQUEST_TB);
  Serial3.write(stopByte); 
  Serial3.write(stopByte);
  delayMicroseconds(500);
  digitalWrite(3,LOW);
}

union{
  int result;
  struct {
    byte low;
    byte high;
  }
  join;
} 
joint;

bool readSAS(){
  static char packet[20];
  if(Serial3.available()>9)    
    if(Serial3.read()==0xEC){ 
      if(Serial3.read()==SASADDRESS){
        switch(Serial3.read()){  //switch the command to be read in     
        case SAS_REQUEST_TB:
          for (int i=0;i<10;i++){
            packet[i]=Serial3.read(); 
          }
          joint.join.high = packet[0];
          joint.join.low = packet[1];
          throttle1 = joint.result;
          joint.join.high = packet[2];
          joint.join.low = packet[3];
          throttle2 = joint.result;
          joint.join.high = packet[4];
          joint.join.low = packet[5];
          brake1 = joint.result;          
          joint.join.high = packet[6];
          joint.join.low = packet[7];
          brake2= joint.result;
          if(packet[8]==0x99 && packet[9]==0x99)
            return true;
          break;
        case SAS_REQUEST_WS:
          speedFrontLeft=(Serial3.read()<<8)&(Serial3.read());
          speedFrontRight=(Serial3.read()<<8)&(Serial3.read());
          speedRearLeft=(Serial3.read()<<8)&(Serial3.read());
          speedRearRight=(Serial3.read()<<8)&(Serial3.read());
          if(Serial3.read()==0x99 && Serial3.read()==0x99)
            return true;        
          break;
        case SAS_REQUEST_ALL:
          for (int i=0;i<18;i++){
            packet[i]=Serial3.read();  
          }
          joint.join.high = packet[0];
          joint.join.low = packet[1];
          throttle1 = joint.result;
          joint.join.high = packet[2];
          joint.join.low = packet[3];
          throttle2 = joint.result;
          joint.join.high = packet[4];
          joint.join.low = packet[5];
          brake1 = joint.result;          
          joint.join.high = packet[6];
          joint.join.low = packet[7];
          brake2= joint.result;
          speedFrontLeft=(packet[8]<<8)&(packet[9]);
          speedFrontRight=(packet[10]<<8)&(packet[11]);
          speedRearLeft=(packet[12]<<8)&(packet[13]);
          speedRearRight=(packet[14]<<8)&(packet[15]);
          if(packet[16]==0x99 && packet[17]==0x99)
            return true;
          break;
        }
      }
      else return false;
    }  
  else{
    delay(5);
    return false;
  }
}

void sendDataMCS(){
  digitalWrite(27,HIGH);
  delayMicroseconds(2500);
  Serial1.write(startByte);
  Serial1.write(MCSADDRESS);
  Serial1.write(MCSUPDATE);
  Serial1.write(((throttle1&0xFF00)>>8));
  Serial1.write((uint8)(throttle1&0x00FF));
  Serial1.write((uint8)((throttle2&0xFF00)>>8));
  Serial1.write((uint8)(throttle2&0x00FF));
  Serial1.write((uint8)((brake1&0xFF00)>>8));
  Serial1.write((uint8)(brake1&0x00FF));
  Serial1.write((uint8)((brake2&0xFF00)>>8));
  Serial1.write((uint8)(brake2&0x00FF));
  Serial1.write(stopByte); 
  Serial1.write(stopByte);
  delayMicroseconds(500); 
  digitalWrite(27,LOW);
}

void readMCS(){
  while(!(Serial1.available())){    
    delay(10);
    unheard++;
    if (unheard>10){
      unheard=0;
      notListening++;
      if (notListening>5){
        notListening=0;        
        faultLock=true; 
        break;
      }
      sendDataMCS();
    } 
  }   
  notListening=0;
  unheard=0;   
  while(Serial1.available())    {    //
    Serial1.read();
  } 
}

void sendStopMCS(){
  digitalWrite(27,HIGH);   
  delayMicroseconds(500);
  Serial1.write(startByte);
  Serial1.write(MCSADDRESS);
  Serial1.write(MCSSTOP);
  Serial1.write(stopByte); 
  Serial1.write(stopByte);
  delayMicroseconds(500);    
  digitalWrite(27,LOW);
}

bool readBMSResponse(){
  if(Serial3.available()>6){
    while(Serial3.available()){    //BMS
      if(faultFlag){
        if(Serial3.read()==startByte)
          if(Serial3.read()==BMSADDRESS)
            if(Serial3.read()==FAULTUPDATE)
              switch(Serial3.read()){
              case 1:
                if(Serial3.read()==0x99)
                  if(Serial3.read()==0x99){  
                   Serial2.println(); 
                   Serial2.println("BMS Fault Pass");
                   Serial2.println(); 
                  }
                break;
              case 0: 
                if(Serial3.read()==0x99)
                  if(Serial3.read()==0x99){   
                    faultLock=true;
                  }
                break;
              }
        faultFlag=0;
        return true;
      }
      else if(Serial3.read()==startByte)
        if(Serial3.read()==BMSADDRESS)
          if(Serial3.read()==BATTERYUPDATE)
            for(int i=0;i<numBatteries;i++){            
              batteryInfo[i]=(Serial3.read());
            }
      if(Serial3.read()==0x99)
        if(Serial3.read()==0x99){                    
          digitalWrite(BOARD_LED_PIN,HIGH);  
        }       

    }
    return true;  
  }
  else{    
    delay(5);
    return false;
  }
}

void requestBMSFaults(){  
  faultFlag=true;  
  digitalWrite(3,HIGH);   
  delayMicroseconds(500); 
  Serial3.write(startByte);
  Serial3.write(BMSADDRESS);
  Serial3.write(FAULTUPDATE);
  Serial3.write(stopByte); 
  Serial3.write(stopByte); 
  delayMicroseconds(500); 
  digitalWrite(3,LOW);
}

void requestFaultUpdate(){
  requestBMSFaults();
  delay(10);

  while(!readBMSResponse()){
    delay(10);    
    unheard++;
    if (unheard>5){
      unheard=0;
      notListening++;
      if (notListening>5){
        notListening=0;  
        faultLock=true;
        break;
      }
      requestBMSFaults();
    } 
  }
  notListening=0;
  unheard=0; 
}

void requestBatteryInformation(){
  batteryFlag=true;
  digitalWrite(3,HIGH);   
  delayMicroseconds(500); 
  Serial3.write(startByte);
  Serial3.write(BMSADDRESS);
  Serial3.write(BATTERYUPDATE);
  Serial3.write(stopByte); 
  Serial3.write(stopByte); 
  delayMicroseconds(500); 
  digitalWrite(3,LOW);
}



bool variableIsAbout(int var1, int var2, int range){
  return ((var1<var2+range)&&(var1<var2-range));
}













