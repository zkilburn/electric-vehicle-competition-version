//COM Serial1 =mcs
//Serial3 = SAS
#define startByte 0xEC
#define stopByte 0x99
int inByte = 0;         // incoming serial byte

void setup() {
  pinMode(BOARD_LED_PIN, OUTPUT);
  Serial1.begin(38400);
  Serial2.begin(38400);
  Serial3.begin(38400);
  pinMode(3,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(27,OUTPUT);
  digitalWrite(3,HIGH);
  digitalWrite(10,HIGH);
  digitalWrite(27,HIGH);
}

void loop() {
  
  establishContact();
  delay(15);
  digitalWrite(3,LOW);
  digitalWrite(27,LOW);
  digitalWrite(10,LOW);
  delay(1000);

  while(Serial3.available())  {   //SAS
    digitalWrite(BOARD_LED_PIN,HIGH);  
    Serial3.read();
  }
  while(Serial1.available())    {    //MCS   
    digitalWrite(BOARD_LED_PIN,HIGH);  
    Serial1.read();
  }
  while(Serial2.available()){    //BMS
    digitalWrite(BOARD_LED_PIN,HIGH);  
    Serial2.read();
  }
  delay(5);  
  digitalWrite(10,HIGH);
  digitalWrite(3,HIGH);  
  digitalWrite(27,HIGH);
     
}

void establishContact() {
  delay(1);
  Serial3.write(startByte);
  Serial3.write(0x01);
  Serial3.write(stopByte); 
  Serial3.write(stopByte); 

  Serial1.write(startByte);
  Serial1.write(0x02);
  Serial3.write(stopByte); 
  Serial3.write(stopByte); 

  Serial2.write(startByte);
  Serial2.write(0x03);
  Serial3.write(stopByte); 
  Serial3.write(stopByte); 
}


