
_initValues:

;SAS2.mbas,57 :: 		sub procedure initValues()
;SAS2.mbas,58 :: 		throttleSample1=0
	CLR	W0
	MOV	W0, _throttleSample1
;SAS2.mbas,59 :: 		throttleSample2=1
	MOV	#1, W0
	MOV	W0, _throttleSample2
;SAS2.mbas,60 :: 		brakeSample1=2
	MOV	#2, W0
	MOV	W0, _brakeSample1
;SAS2.mbas,61 :: 		brakeSample2=3
	MOV	#3, W0
	MOV	W0, _brakeSample2
;SAS2.mbas,62 :: 		speedSample1=4
	MOV	#4, W0
	MOV	W0, _speedSample1
;SAS2.mbas,63 :: 		speedSample2=5
	MOV	#5, W0
	MOV	W0, _speedSample2
;SAS2.mbas,64 :: 		speedSample3=6
	MOV	#6, W0
	MOV	W0, _speedSample3
;SAS2.mbas,65 :: 		speedsample4=7
	MOV	#7, W0
	MOV	W0, _speedSample4
;SAS2.mbas,66 :: 		PACKETREADY=0
	MOV	#lo_addr(_PACKETREADY), W1
	CLR	W0
	MOV.B	W0, [W1]
;SAS2.mbas,67 :: 		end sub
L_end_initValues:
	RETURN
; end of _initValues

_writeSensor:

;SAS2.mbas,69 :: 		sub procedure writeSensor(dim toSend as word)
;SAS2.mbas,71 :: 		lowb=toSend and 0x00FF
	PUSH	W10
	MOV.B	#255, W1
	MOV	#lo_addr(_lowb), W0
	AND.B	W10, W1, [W0]
;SAS2.mbas,72 :: 		highb=(toSend >> 8) and 0x00FF
	LSR	W10, #8, W2
	MOV.B	#255, W1
	MOV	#lo_addr(_highb), W0
	AND.B	W2, W1, [W0]
;SAS2.mbas,73 :: 		UART2_write(highb)
	MOV	#lo_addr(_highb), W0
	ZE	[W0], W10
	CALL	_UART2_Write
;SAS2.mbas,74 :: 		UART2_write(lowb)
	MOV	#lo_addr(_lowb), W0
	ZE	[W0], W10
	CALL	_UART2_Write
;SAS2.mbas,75 :: 		end sub
L_end_writeSensor:
	POP	W10
	RETURN
; end of _writeSensor

_writeStart:

;SAS2.mbas,77 :: 		sub procedure writeStart()
;SAS2.mbas,78 :: 		LATA.9=1
	PUSH	W10
	BSET	LATA, #9
;SAS2.mbas,79 :: 		delay_ms(25)
	MOV	#3, W8
	MOV	#52261, W7
L__writeStart3:
	DEC	W7
	BRA NZ	L__writeStart3
	DEC	W8
	BRA NZ	L__writeStart3
;SAS2.mbas,80 :: 		UART2_write(0xEC)
	MOV	#236, W10
	CALL	_UART2_Write
;SAS2.mbas,81 :: 		UART2_WRITE(0X01)
	MOV	#1, W10
	CALL	_UART2_Write
;SAS2.mbas,82 :: 		end sub
L_end_writeStart:
	POP	W10
	RETURN
; end of _writeStart

_writeEnd:

;SAS2.mbas,83 :: 		sub procedure writeEnd()
;SAS2.mbas,84 :: 		UART2_write(0x99)
	PUSH	W10
	MOV	#153, W10
	CALL	_UART2_Write
;SAS2.mbas,85 :: 		UART2_write(0x99)
	MOV	#153, W10
	CALL	_UART2_Write
;SAS2.mbas,86 :: 		delay_ms(50)
	MOV	#6, W8
	MOV	#38987, W7
L__writeEnd6:
	DEC	W7
	BRA NZ	L__writeEnd6
	DEC	W8
	BRA NZ	L__writeEnd6
	NOP
;SAS2.mbas,87 :: 		LATA.9=0
	BCLR	LATA, #9
;SAS2.mbas,88 :: 		end sub
L_end_writeEnd:
	POP	W10
	RETURN
; end of _writeEnd

_RS485_Slave_Init:

;SAS2.mbas,90 :: 		sub procedure RS485_Slave_Init()
;SAS2.mbas,91 :: 		LATC.4=1
	PUSH	W10
	PUSH	W11
	BSET	LATC, #4
;SAS2.mbas,92 :: 		LATC.3=1
	BSET	LATC, #3
;SAS2.mbas,93 :: 		UART2_Init(38400)
	MOV	#38400, W10
	MOV	#0, W11
	CALL	_UART2_Init
;SAS2.mbas,94 :: 		end sub
L_end_RS485_Slave_Init:
	POP	W11
	POP	W10
	RETURN
; end of _RS485_Slave_Init

_USB_init:

;SAS2.mbas,96 :: 		sub procedure USB_init()
;SAS2.mbas,97 :: 		UART1_Init(38400)
	PUSH	W10
	PUSH	W11
	MOV	#38400, W10
	MOV	#0, W11
	CALL	_UART1_Init
;SAS2.mbas,98 :: 		end sub
L_end_USB_init:
	POP	W11
	POP	W10
	RETURN
; end of _USB_init

_init_Pins:

;SAS2.mbas,99 :: 		sub procedure init_Pins()
;SAS2.mbas,100 :: 		Unlock_IOLOCK()
	PUSH	W10
	PUSH	W11
	PUSH	W12
	CALL	_Unlock_IOLOCK
;SAS2.mbas,101 :: 		PPS_Mapping_NoLock(USB_Tx, _INPUT, _U1RX)   ' Sets pin 2 of PIC as USB RX input
	MOV.B	#11, W12
	MOV.B	#1, W11
	MOV.B	#22, W10
	CALL	_PPS_Mapping_NoLock
;SAS2.mbas,102 :: 		PPS_Mapping_NoLock(USB_Tx, _OUTPUT, _U1TX)  ' Sets pin 3 of PIC as USB TX output
	MOV.B	#3, W12
	CLR	W11
	MOV.B	#22, W10
	CALL	_PPS_Mapping_NoLock
;SAS2.mbas,103 :: 		PPS_Mapping_NoLock(RS485_Rx, _INPUT, _U2RX)   ' Sets pin 4 of PIC as RS485 RX input
	MOV.B	#13, W12
	MOV.B	#1, W11
	MOV.B	#20, W10
	CALL	_PPS_Mapping_NoLock
;SAS2.mbas,104 :: 		PPS_Mapping_NoLock(RS485_Tx, _OUTPUT, _U2TX)  ' Sets pin 5 of PIC as RS485 TX output
	MOV.B	#5, W12
	CLR	W11
	MOV.B	#19, W10
	CALL	_PPS_Mapping_NoLock
;SAS2.mbas,109 :: 		Lock_IOLOCK()
	CALL	_Lock_IOLOCK
;SAS2.mbas,110 :: 		delay_ms(5)
	MOV	#36666, W7
L__init_Pins11:
	DEC	W7
	BRA NZ	L__init_Pins11
	NOP
	NOP
;SAS2.mbas,111 :: 		TRISB.12=0
	BCLR	TRISB, #12
;SAS2.mbas,112 :: 		TRISA.9=0
	BCLR	TRISA, #9
;SAS2.mbas,113 :: 		TRISA.4=0
	BCLR	TRISA, #4
;SAS2.mbas,114 :: 		TRISA.1=1
	BSET	TRISA, #1
;SAS2.mbas,115 :: 		TRISA.0=1
	BSET	TRISA, #0
;SAS2.mbas,116 :: 		TRISB.2=1
	BSET	TRISB, #2
;SAS2.mbas,117 :: 		TRISB.3=1
	BSET	TRISB, #3
;SAS2.mbas,118 :: 		TRISC.0=1
	BSET	TRISC, #0
;SAS2.mbas,119 :: 		TRISC.1=1
	BSET	TRISC, #1
;SAS2.mbas,120 :: 		LATC.7=1
	BSET	LATC, #7
;SAS2.mbas,121 :: 		LATC.6=1
	BSET	LATC, #6
;SAS2.mbas,122 :: 		LATC.3=1
	BSET	LATC, #3
;SAS2.mbas,123 :: 		LATC.4=1
	BSET	LATC, #4
;SAS2.mbas,124 :: 		LATA.9=0
	BCLR	LATA, #9
;SAS2.mbas,125 :: 		LATA.4=0
	BCLR	LATA, #4
;SAS2.mbas,126 :: 		LATB.12=1
	BSET	LATB, #12
;SAS2.mbas,127 :: 		AD1PCFGL=0XFF00
	MOV	#65280, W0
	MOV	WREG, AD1PCFGL
;SAS2.mbas,129 :: 		end sub
L_end_init_Pins:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _init_Pins

_blinkLED:

;SAS2.mbas,130 :: 		sub procedure blinkLED
;SAS2.mbas,131 :: 		if LATB.12=0 then
	BTSC	LATB, #12
	GOTO	L__blinkLED15
;SAS2.mbas,132 :: 		LATB.12=1
	BSET	LATB, #12
	GOTO	L__blinkLED16
;SAS2.mbas,133 :: 		else
L__blinkLED15:
;SAS2.mbas,134 :: 		LATB.12=0
	BCLR	LATB, #12
;SAS2.mbas,135 :: 		end if
L__blinkLED16:
;SAS2.mbas,137 :: 		end sub
L_end_blinkLED:
	RETURN
; end of _blinkLED

_MASTER_INTERRUPT:
	LNK	#2
	PUSH	52
	PUSH	RCOUNT
	PUSH	W0
	MOV	#2, W0
	REPEAT	#12
	PUSH	[W0++]

;SAS2.mbas,141 :: 		sub procedure MASTER_INTERRUPT() org IVT_ADDR_U2RXINTERRUPT
;SAS2.mbas,142 :: 		Uart2rxBuf[Uart2rxHead] = UART2_Read()  'put char in buf
	PUSH	W10
	PUSH	W11
	MOV	#lo_addr(_Uart2rxHead), W0
	ZE	[W0], W1
	MOV	#1, W0
	SL	W1, W0, W1
	MOV	#lo_addr(_Uart2rxBuf), W0
	ADD	W0, W1, W0
	MOV	W0, [W14+0]
	CALL	_UART2_Read
	MOV	[W14+0], W1
	MOV	W0, [W1]
;SAS2.mbas,143 :: 		BYTETOSTR(uart2rxbuf[uart2rxhead],TXT)
	MOV	#lo_addr(_Uart2rxHead), W0
	ZE	[W0], W1
	MOV	#1, W0
	SL	W1, W0, W1
	MOV	#lo_addr(_Uart2rxBuf), W0
	ADD	W0, W1, W0
	MOV	#lo_addr(_txt), W11
	MOV.B	[W0], W10
	CALL	_ByteToStr
;SAS2.mbas,144 :: 		uart1_write_text(TXT)
	MOV	#lo_addr(_txt), W10
	CALL	_UART1_Write_Text
;SAS2.mbas,145 :: 		Uart2rxHead=uart2rxhead+1
	MOV.B	#1, W1
	MOV	#lo_addr(_Uart2rxHead), W0
	ADD.B	W1, [W0], [W0]
;SAS2.mbas,161 :: 		IFS1.U2RXIF=0  'clear U1RXIF
	BCLR	IFS1, #14
;SAS2.mbas,162 :: 		end sub
L_end_MASTER_INTERRUPT:
	POP	W11
	POP	W10
	MOV	#26, W0
	REPEAT	#12
	POP	[W0--]
	POP	W0
	POP	RCOUNT
	POP	52
	ULNK
	RETFIE
; end of _MASTER_INTERRUPT

_main:
	MOV	#2048, W15
	MOV	#6142, W0
	MOV	WREG, 32
	MOV	#1, W0
	MOV	WREG, 52
	MOV	#4, W0
	IOR	68
	LNK	#28

;SAS2.mbas,165 :: 		main:
;SAS2.mbas,167 :: 		CLKDIV=CLKDIV AND 0xFFE5
	PUSH	W10
	MOV	#65509, W1
	MOV	#lo_addr(CLKDIV), W0
	AND	W1, [W0], [W0]
;SAS2.mbas,168 :: 		PLLFBD=12
	MOV	#12, W0
	MOV	WREG, PLLFBD
;SAS2.mbas,170 :: 		u2mode.brgh=0
	BCLR	U2MODE, #3
;SAS2.mbas,171 :: 		U2RXIF_bit = 0              ' ensure interrupt not pending
	BCLR	U2RXIF_bit, BitPos(U2RXIF_bit+0)
;SAS2.mbas,172 :: 		U2RXIE_bit = 1              ' enable intterupt                         1
	BSET	U2RXIE_bit, BitPos(U2RXIE_bit+0)
;SAS2.mbas,174 :: 		init_Pins()
	CALL	_init_Pins
;SAS2.mbas,175 :: 		RS485_Slave_Init()
	CALL	_RS485_Slave_Init
;SAS2.mbas,176 :: 		USB_Init()
	CALL	_USB_init
;SAS2.mbas,179 :: 		ADC1_Init()
	CALL	_ADC1_Init
;SAS2.mbas,180 :: 		UART1_write(13)
	MOV	#13, W10
	CALL	_UART1_Write
;SAS2.mbas,181 :: 		UART1_write_text("Welcome to the danger zone")
	ADD	W14, #0, W1
	MOV.B	#87, W0
	MOV.B	W0, [W1++]
	MOV.B	#101, W0
	MOV.B	W0, [W1++]
	MOV.B	#108, W0
	MOV.B	W0, [W1++]
	MOV.B	#99, W0
	MOV.B	W0, [W1++]
	MOV.B	#111, W0
	MOV.B	W0, [W1++]
	MOV.B	#109, W0
	MOV.B	W0, [W1++]
	MOV.B	#101, W0
	MOV.B	W0, [W1++]
	MOV.B	#32, W0
	MOV.B	W0, [W1++]
	MOV.B	#116, W0
	MOV.B	W0, [W1++]
	MOV.B	#111, W0
	MOV.B	W0, [W1++]
	MOV.B	#32, W0
	MOV.B	W0, [W1++]
	MOV.B	#116, W0
	MOV.B	W0, [W1++]
	MOV.B	#104, W0
	MOV.B	W0, [W1++]
	MOV.B	#101, W0
	MOV.B	W0, [W1++]
	MOV.B	#32, W0
	MOV.B	W0, [W1++]
	MOV.B	#100, W0
	MOV.B	W0, [W1++]
	MOV.B	#97, W0
	MOV.B	W0, [W1++]
	MOV.B	#110, W0
	MOV.B	W0, [W1++]
	MOV.B	#103, W0
	MOV.B	W0, [W1++]
	MOV.B	#101, W0
	MOV.B	W0, [W1++]
	MOV.B	#114, W0
	MOV.B	W0, [W1++]
	MOV.B	#32, W0
	MOV.B	W0, [W1++]
	MOV.B	#122, W0
	MOV.B	W0, [W1++]
	MOV.B	#111, W0
	MOV.B	W0, [W1++]
	MOV.B	#110, W0
	MOV.B	W0, [W1++]
	MOV.B	#101, W0
	MOV.B	W0, [W1++]
	CLR	W0
	MOV.B	W0, [W1++]
	ADD	W14, #0, W0
	MOV	W0, W10
	CALL	_UART1_Write_Text
;SAS2.mbas,182 :: 		initValues()
	CALL	_initValues
;SAS2.mbas,184 :: 		while true
L__main20:
;SAS2.mbas,190 :: 		CYCLES=CYCLES+1
	MOV	_cycles, W2
	MOV	_cycles+2, W3
	MOV	#0, W0
	MOV	#16256, W1
	CALL	__AddSub_FP
	MOV	W0, _cycles
	MOV	W1, _cycles+2
;SAS2.mbas,191 :: 		if(cycles>10000) then
	MOV	#16384, W2
	MOV	#17948, W3
	CALL	__Compare_Ge_Fp
	CP0	W0
	CLR	W0
	BRA LE	L__main62
	COM	W0
L__main62:
	CP0	W0
	BRA NZ	L__main63
	GOTO	L__main25
L__main63:
;SAS2.mbas,192 :: 		cycles=0
	CLR	W0
	CLR	W1
	MOV	W0, _cycles
	MOV	W1, _cycles+2
;SAS2.mbas,202 :: 		advancedCycles=advancedCycles+1
	MOV	#1, W1
	MOV	#lo_addr(_advancedCycles), W0
	ADD	W1, [W0], [W0]
;SAS2.mbas,203 :: 		if PACKETREADY=1 then
	MOV	#lo_addr(_PACKETREADY), W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__main64
	GOTO	L__main28
L__main64:
;SAS2.mbas,204 :: 		PACKETREADY=0
	MOV	#lo_addr(_PACKETREADY), W1
	CLR	W0
	MOV.B	W0, [W1]
;SAS2.mbas,205 :: 		Uart2rxHead=0
	MOV	#lo_addr(_Uart2rxHead), W1
	CLR	W0
	MOV.B	W0, [W1]
;SAS2.mbas,206 :: 		if (Uart2rxBuf[0]=0xEC) then
	MOV	#236, W1
	MOV	#lo_addr(_Uart2rxBuf), W0
	CP	W1, [W0]
	BRA Z	L__main65
	GOTO	L__main31
L__main65:
;SAS2.mbas,207 :: 		if (Uart2rxBuf[1]=0X01) then
	MOV	_Uart2rxBuf+2, W0
	CP	W0, #1
	BRA Z	L__main66
	GOTO	L__main34
L__main66:
;SAS2.mbas,208 :: 		if (Uart2rxBuf[3]=0x99)  then
	MOV	#153, W1
	MOV	#lo_addr(_Uart2rxBuf+6), W0
	CP	W1, [W0]
	BRA Z	L__main67
	GOTO	L__main37
L__main67:
;SAS2.mbas,209 :: 		if (Uart2rxBuf[4]=0x99) then
	MOV	#153, W1
	MOV	#lo_addr(_Uart2rxBuf+8), W0
	CP	W1, [W0]
	BRA Z	L__main68
	GOTO	L__main40
L__main68:
;SAS2.mbas,211 :: 		writeStart()
	CALL	_writeStart
;SAS2.mbas,212 :: 		uart2_write(Uart2rxBuf[2])
	MOV	_Uart2rxBuf+4, W10
	CALL	_UART2_Write
;SAS2.mbas,214 :: 		case 1
	MOV	_Uart2rxBuf+4, W0
	CP	W0, #1
	BRA Z	L__main69
	GOTO	L__main45
L__main69:
;SAS2.mbas,215 :: 		writeSensor(throttleSample1)
	MOV	_throttleSample1, W10
	CALL	_writeSensor
;SAS2.mbas,216 :: 		writeSensor(throttleSample2)
	MOV	_throttleSample2, W10
	CALL	_writeSensor
;SAS2.mbas,217 :: 		writeSensor(brakeSample1)
	MOV	_brakeSample1, W10
	CALL	_writeSensor
;SAS2.mbas,218 :: 		writeSensor(brakeSample2)
	MOV	_brakeSample2, W10
	CALL	_writeSensor
	GOTO	L__main42
L__main45:
;SAS2.mbas,219 :: 		case 2
	MOV	_Uart2rxBuf+4, W0
	CP	W0, #2
	BRA Z	L__main70
	GOTO	L__main48
L__main70:
;SAS2.mbas,220 :: 		writeSensor(speedSample1)
	MOV	_speedSample1, W10
	CALL	_writeSensor
;SAS2.mbas,221 :: 		writeSensor(speedSample2)
	MOV	_speedSample2, W10
	CALL	_writeSensor
;SAS2.mbas,222 :: 		writeSensor(speedSample3)
	MOV	_speedSample3, W10
	CALL	_writeSensor
;SAS2.mbas,223 :: 		writeSensor(speedSample4)
	MOV	_speedSample4, W10
	CALL	_writeSensor
	GOTO	L__main42
L__main48:
;SAS2.mbas,224 :: 		case 3
	MOV	_Uart2rxBuf+4, W0
	CP	W0, #3
	BRA Z	L__main71
	GOTO	L__main51
L__main71:
;SAS2.mbas,225 :: 		writeSensor(throttleSample1)
	MOV	_throttleSample1, W10
	CALL	_writeSensor
;SAS2.mbas,226 :: 		writeSensor(throttleSample2)
	MOV	_throttleSample2, W10
	CALL	_writeSensor
;SAS2.mbas,227 :: 		writeSensor(brakeSample1)
	MOV	_brakeSample1, W10
	CALL	_writeSensor
;SAS2.mbas,228 :: 		writeSensor(brakeSample2)
	MOV	_brakeSample2, W10
	CALL	_writeSensor
;SAS2.mbas,229 :: 		writeSensor(speedSample1)
	MOV	_speedSample1, W10
	CALL	_writeSensor
;SAS2.mbas,230 :: 		writeSensor(speedSample2)
	MOV	_speedSample2, W10
	CALL	_writeSensor
;SAS2.mbas,231 :: 		writeSensor(speedSample3)
	MOV	_speedSample3, W10
	CALL	_writeSensor
;SAS2.mbas,232 :: 		writeSensor(speedSample4)
	MOV	_speedSample4, W10
	CALL	_writeSensor
	GOTO	L__main42
L__main51:
L__main42:
;SAS2.mbas,234 :: 		writeEnd()
	CALL	_writeEnd
L__main40:
;SAS2.mbas,235 :: 		end if
L__main37:
;SAS2.mbas,236 :: 		end if
L__main34:
;SAS2.mbas,237 :: 		end if
L__main31:
;SAS2.mbas,238 :: 		end if
L__main28:
;SAS2.mbas,240 :: 		blinkLED
	CALL	_blinkLED
L__main25:
;SAS2.mbas,242 :: 		wend
	GOTO	L__main20
L_end_main:
	POP	W10
	ULNK
L__main_end_loop:
	BRA	L__main_end_loop
; end of _main
