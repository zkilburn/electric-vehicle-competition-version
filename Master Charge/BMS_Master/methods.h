/* 
 * File:   methods.h
 * Author: User
 *
 * Created on April 6, 2014, 4:46 PM
 */

#ifndef METHODS_H
#define	METHODS_H

#ifdef	__cplusplus
extern "C" {
#endif
//function declarations
void initRS485Direction();
void initInterrupts();
void initPins();
void initUARTS();
void setupCommData();
void startSlaveTalk();
void checkSlaveComms();
void writeSlaveComms();
void toggleLED();
void delay_5ms();
void delay_1s();
void oscillatorInit();
void putrsUART2(const unsigned char *data);
void putByteUART2(unsigned char data);
void listenECU();
void toggleECUdirection();
#ifdef	__cplusplus
}
#endif
#endif	/* METHODS_H */