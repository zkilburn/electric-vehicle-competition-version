/* 
 * File:   definitions.h
 * Author: User
 *
 * Created on April 6, 2014, 4:45 PM
 */

#ifndef DEFINITIONS_H
#define	DEFINITIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define startByte 0xEC
#define stopByte 0x99
#define address 0x03

//asm delay function -- should work :)
#define DELAY_100uS asm volatile ("REPEAT, #2500"); Nop();
//some constant declarations
#define numSlaves 28
#define LEDT 2000  //200 counts of 5ms till LED toggle (~1 second)
#define ST 1 //50 counts of 5ms for slave retransmit (~250 ms

//Baud rate generator help
#define FP 12500000
#define BAUDRATE384 38400
#define BAUDRATE96 9600
#define BRGVAL ((FP/BAUDRATE384)/16)-1
#define BRGVAL2 ((FP/BAUDRATE96)/16)-1

//READABILITY DEFINES
#define INPUT 1
#define OUTPUT 0
#define HIGH 1
#define LOW 0
#define TALK 1
#define LISTEN 0
#define NTC TRISBbits.TRISB2

//OUTPUT COMMAND
#define LED LATBbits.LATB11 //SIGN OF LIFE LED
#define L_T2 LATCbits.LATC9 //SLAVE DIRECTION (LISTEN/TALK)
#define L_T1 LATAbits.LATA9 //ECU DIRECTION (LISTEN/TALK)
#define RELAY_ENABLE LATAbits.LATA8
#define RELAY_LED LATBbits.LATB0
#define WatchDog LATAbits.LATA1
//UART Definitions
#define U1Rx_RPn RPINR18bits.U1RXR
#define U2Rx_RPn RPINR19bits.U2RXR
#define RP12map RPOR6bits.RP12R
#define RP13map RPOR6bits.RP13R
#define RP19map RPOR9bits.RP19R
#define RP20map RPOR10bits.RP20R
#define RP22map RPOR11bits.RP22R
#define RP23map RPOR11bits.RP23R
#define U1Tx_RPn 3
#define U2Tx_RPn 5

//INTERRUPT0 pin config
#define Interrupt0Pin RPINR0bits.INT1R



#endif	/* DEFINITIONS_H */

