/* 
 * File:   interrupts.h
 * Author: User
 *
 * Created on April 6, 2014, 4:46 PM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C" {
#endif
    int battWrite=1;
    unsigned int battTime=0;

    void writeBatteryVolts(int number,int volts){
        char checksum = 0;
        putByteUART1(0x01);
        checksum =0x01;
        putByteUART1(0x0F);
        checksum ^=0x0F;
        putByteUART1(number);
        checksum ^=number;
        putByteUART1(0x00);
        checksum ^= 0x00;
        putByteUART1(volts);
        checksum ^= volts;
        putByteUART1(checksum);

    }
    void initInterrupts() {
        INTCON1bits.NSTDIS = 1; //no nesting of interrupts
        IFS0bits.U1TXIF = 0;
        IFS1bits.U2TXIF = 0;
        IFS0bits.U1RXIF = 0;
        IFS1bits.U2RXIF = 0;
        IFS0bits.INT0IF = 0;
    }

    void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void) {
        //        if (ECUbyteNum < ECUpacketSize) {
        //            DELAY_100uS;
        //            U1TXREG = ECUOut[ECUbyteNum];
        //            ECUbyteNum++;
        //        } else {
        //            responseSent = 1;
        //            ECUbyteNum = 1;
        //            L_T1 = LISTEN;
        //        }
        //delay_5ms();
        //U1TXREG = ECUOut[0];
        IFS0bits.U1TXIF = 0;
    }

    //    void __attribute__((interrupt, no_auto_psv)) _U2TXInterrupt(void) {
    ////    if (slaveByteNum < slavePacketSize) {
    ////        U2TXREG = SlaveOut[slaveByteNum];
    ////        slaveByteNum++;
    ////    } else {
    ////        slaveByteNum = 1;
    ////        slaveSent = 1;
    ////        L_T2=!LISTEN;
    ////    }
    //        slaveSent=1;
    //        //putrsUART2("Test");
    //        IFS1bits.U2TXIF = 0;
    //    }
    //For when the ECU comes knocking

    void addByte(unsigned char incomming) {
        if (!startHeard) { //if you havent heard a start byte yet
            if (incomming == startByte) //and the incomming is the start byte
            {
                startHeard = 1; //set flag
                return; //bounce
            } else return; //if its not start byte and we havent heard one ignor it

        } else //else we have had our start byte
        {
            if (!me) //if we havent heard if its us after start byte
            {
                if (incomming == address) // if it is us
                    me = 1; //set flag
                else //else
                {
                    startHeard = 0;
                    return;
                }
            } else {
                if (checkBatteryRequest) {
                    batteryToCheck = incomming;
                    checkBatteryRequest = 0;
                }
                if (incomming == stopByte && stop1) { //if this is second stop byte in a row terminate packet read
                    packetReady = 1;
                    startHeard = 0;
                    stop1 = 0;
                    me = 0;
                    return;
                } else if (incomming == stopByte) //else if this is a stopByte w/o heard one yet
                {
                    stop1 = 1;
                    return;
                }
                switch (incomming) { //fill packet
                    case 1:
                        inputCmd = 1;
                        break;
                    case 2:
                        inputCmd = 2;
                        break;
                    case 3:
                        inputCmd = 3;
                        checkBatteryRequest = true;
                        break;
                        return;
                }
            }
        }
    }

    void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void) {
        while (U1STAbits.URXDA == 1) {
            addByte(U1RXREG);
        }
        IFS0bits.U1RXIF = 0;
    }
    //For when a slave knocks

    void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void) {
        static int responseAddress;
        switch (heard) {
            case 0:
                if (U2RXREG == 0xEC)
                    heard++;
                break;
            case 1:
                responseAddress = U2RXREG;
                if (responseAddress == slaveAddr) {
                    heard++;
                } else heard = 0;
                break;
            case 2:
                if (responseAddress != 28) {
                    batteryVolts[responseAddress] = U2RXREG;
                    heard++;
                    if(battWrite==responseAddress){
                        if(battTime>10){
                            writeBatteryVolts(responseAddress,batteryVolts[responseAddress]);
                            battTime=0;
                            if(battWrite>numSlaves-1){
                                battWrite=1;
                            }else
                                battWrite++;
                        }else
                            battTime++;

                    }
                    
                } else {
                    currentReadH = U2RXREG;
                    ECUOut[4]=currentReadH;
                    heard ++;
                }
                break;
            case 3:

                if (responseAddress != 28) {
                batteryTemp[responseAddress] = U2RXREG;
                heard++;
                }
                else{
                    currentReadL=U2RXREG;
                    ECUOut[5]=currentReadL;
                heard++;
                }
                break;
            case 4:
                if (responseAddress != 28) {
                    if (U2RXREG == 0x0F) {
                        bypassRecord[responseAddress] = 1;
                    } else {
                        bypassRecord[responseAddress] = 0;
                    }
                } else{
                    currentError = U2RXREG;
//                    if(currentError==0){
//                        faultPass=0;
//                        ECUOut[3]=faultPass;
//                    }
                }
                heard++;
                break;
            case 5:
                if (U2RXREG == 0x99)
                    CRC1 = true;
                heard++;
                break;
            case 6:
                if (U2RXREG == 0x99 && CRC1) {
                    slaveSent = 1;
                    slaveAddr++;

                }
                heard = 0;
                break;
        }
        //    LED=!LED;

        IFS1bits.U2RXIF = 0;
    }

    void __attribute__((interrupt, auto_psv)) _INT0Interrupt(void) {
        IFS0bits.INT0IF = 0;
    }

#ifdef	__cplusplus
}
#endif
#endif
