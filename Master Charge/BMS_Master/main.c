/* 
 * File:   main.c
 * Author: pwnedmark (revised by bozzobrain)
 *
 * Created on March 29, 2014, 4:35 AM (Reborn on April 3rd, 2014 1:31 AM)
 *
 * BMS master will provide a constant monitoring of the cells it slaves
 *  -- cells can interrupt the master on interrupt0 flagging a need to be serviced
 *  -- three posible UARTS to connect to (we have 2 modules)
 *      1- ECU
 *      2- Slaves
 *      3- USB
 *  -- there is an NTC located on B2 (an4?)
 *  --relay control for safety I assume
 *  --
 */

#include <stdlib.h>
#include <xc.h>
#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "definitions.h"
#include "methods.h"
#include "variables.h"
#include "initSystems.h"
#include "interrupts.h"
#include "commSystems.h"

/*
 * 
 */

int main(int argc, char** argv) {
    initPins();
    oscillatorInit();
    initInterrupts();
    setupCommData();
    initUARTS();
    initRS485Direction();
    delay_5ms();
    while (1) {
        putByteUART1(inputCmd);
               // writeSlaveComms(slaveAddr);
        checkSlaveComms();
        listenECU();
        toggleLED();
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
    }
    return (EXIT_SUCCESS);
}

void listenECU() {
    //if (LEDTime > LEDT) {
    if (packetReady) {
        toggleECUdirection();
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        switch (inputCmd) {
            case 1:
                for (i = 0; i < ECUpacketSize; i++) {
                    putByteUART1(ECUOut[i]);
                    DELAY_100uS;
                }
                break;
            default:
                for (i = 0; i < ECUpacketSize; i++) {
                    if (i == 2) {
                        putByteUART1(inputCmd);
                    } else if (i == 3) {
                        switch (inputCmd) {
                            case 1:
                                putByteUART1(ECUOut[i]);
                                break;
                            case 2:
                                for (i = 0; i < numSlaves; i++){
                                    putByteUART1(batteryVolts[i]);
                                    putByteUART1(batteryTemp[i]);
                                }
                                break;
                            case 3:
                                putByteUART1(batteryVolts[batteryToCheck]);
                                break;
                            default:
                                 putByteUART1(ECUOut[i]);
                        }
                    } else
                        putByteUART1(ECUOut[i]);
                }
        }
        batteryToCheck = 0;
        inputCmd = 0;
        packetReady = 0;

        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;
        toggleECUdirection();
    } // else LEDTime++;
}

void toggleLED() {
    if (LEDTime > LEDT) {
//        putByteUART1(13);
//        for (i = 1; i <= numSlaves; i++) {
//            putByteUART1((char)batteryVolts[i]);
//            putByteUART1(batteryTemp[i]);
//            putByteUART1(bypassRecord[i]);
//            putByteUART1(13);
//        }
//        putByteUART1(currentRead);
//        putByteUART1(currentError);
//        putByteUART1(13);
//        putByteUART1(13);
RELAY_ENABLE=!RELAY_ENABLE;
RELAY_LED=!RELAY_LED;
        LED = !LED;
        LEDTime = 0;
    } else LEDTime++;
}

