/* 
 * File:   commSystems.h
 * Author: User
 *
 * Created on April 8, 2014, 6:17 AM
 */

#ifndef COMMSYSTEMS_H
#define	COMMSYSTEMS_H

#ifdef	__cplusplus
extern "C" {
#endif

    enum slaveSend {
        startB = 0, addr = 1, stopB1 = 2, stopB2 = 3, MAXENUMS = 4
    } sendPoint;

    struct slaveOut {
        unsigned char startB;
        char *addr;
        unsigned char override;
        unsigned char stopB1;
        unsigned char stopB2;
    } SlaveOut;

    void setupCommData() {

        //bare ECU minimum packet contents (header)(says boot not complete

        ECUOut[0] = startByte;
        ECUOut[1] = address;
        ECUOut[2] = 0x01;
        ECUOut[3] = faultPass;
        ECUOut[4] = currentReadH;
        ECUOut[5] = currentReadL;
        ECUOut[6] = stopByte;
        ECUOut[7] = stopByte;
        ECUpacketSize = 8;

        //Testing Slave packet sending
        SlaveOut.startB = startByte;
        SlaveOut.addr = &slaveAddr;
        SlaveOut.override=0x00;
        SlaveOut.stopB1 = stopByte;
        SlaveOut.stopB2 = stopByte;
        slavePacketSize = 5;
    }
    char timeOut = 0;

    void checkSlaveComms() {

        if (slaveTime > ST) { //replace sent with recieved
            if (slaveSent) {
                if (slaveAddr > numSlaves) {
                    slaveAddr = 1;
                    //toggleLED();
                }
//                if(slaveAddr==24){
//                  slaveAddr++;
//                }


//                static bool instructed=false;
//                static int bypassInstructor=1;
//
//                if((slaveAddr==bypassInstructor)&&!instructed){
//                    SlaveOut.override=0xBB;
//                    instructed=true;
//                }
//                else if((slaveAddr==bypassInstructor) && instructed){
//                    SlaveOut.override=0xBA;
//                    instructed=false;
//                }
//                else{
//                    SlaveOut.override=0xBA;
//                }

//
//                if(slaveAddr==27){
//                    SlaveOut.override=0xBA;
//                }
//                else
//                    SlaveOut.override=0;

//

                writeSlaveComms(slaveAddr);

//
//                if(!instructed){
//
//                bypassInstructor++;
//                }
//                if(bypassInstructor> 27){
//                    bypassInstructor=1;
//                }
                slaveSent = 0;
                timeOut = 0;
            } else {
                timeOut++;
                if (timeOut > 100) {
//                        faultPass = 0;
//                        ECUOut[3] = faultPass;
                    writeSlaveComms(slaveAddr);
                    timeOut = 0;
                    slaveSent = 0;
                }
                }
            slaveTime = 0;

        } else slaveTime++;
    }

    void writeSlaveComms(int slaveAddress) {
        L_T2 = !TALK;
        DELAY_100uS;
        putByteUART2(SlaveOut.startB);
        DELAY_100uS;
        putByteUART2(slaveAddress);
        DELAY_100uS;
        putByteUART2(SlaveOut.override);
        DELAY_100uS;
        putByteUART2(SlaveOut.stopB1);
        DELAY_100uS;
        putByteUART2(SlaveOut.stopB2);
        DELAY_100uS;
        DELAY_100uS;
        DELAY_100uS;

        L_T2 = !LISTEN;
    }

    void putrsUART2(const unsigned char *data) {
        do {
            while (U2STAbits.UTXBF);
            U2TXREG = *data;
        } while (*data++);
    }

    void putByteUART2(unsigned char data) {
        while (U2STAbits.UTXBF);
        U2TXREG = data;
    }

    void putByteUART1(unsigned char data) {
        while (U1STAbits.UTXBF);
        U1TXREG = data;
    }

    void toggleECUdirection() {
        DELAY_100uS;
        L_T1 = !L_T1;
        DELAY_100uS;
    }

#ifdef	__cplusplus
}
#endif

#endif	/* COMMSYSTEMS_H */

