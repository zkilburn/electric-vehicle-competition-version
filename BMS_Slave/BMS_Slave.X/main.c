/*
 * File:   newmain.c
 * Author: Rick
 * BMS Slave
 * Created on January 11, 2014, 12:18 AM
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#define _XTAL_FREQ 4000000
#define LED PORTCbits.RC7
#define BYPASS PORTCbits.RC2
#define VOLTBP 38
#define VOLTBPOF 36
#define LEDTCOK 50
#define LEDTCNO 5
#define Timeout 1000   //Comm timeout
#define startByte 0xEC
#define stopByte 0x99
#define incommingLength 4
#define address 1
#define LED PORTCbits.RC7
#define RS485_IO PORTBbits.RB6

//asm delay function -- should work :)
#define DELAY_100uS asm volatile ("REPEAT, #2500"); Nop();


unsigned char a = 33, b = 0, udata;
float ADC = 0;
int bpon = 0; //bypas on or off
int datahere = 0;
int timecount = 0;
float volt = 0;
float temp = 0;
long counter = 0;
long laston = 0;
int LEDtime = 0;
int BPON = 0;
unsigned char ADCStringVal[5];
unsigned char data[5];
bool heard = false;
bool callByte = false;
int me;
int Data1 = 0;
int Data2 = 0;
int Data3 = 0;
//int address = 0;

/*
 *
 */

bool stop1 = false;


void addByte(unsigned char incomming);
void checkAndRespond();
void putrs1USART(const char *data);
void putrsUSART(const char *data);
void putByteUSART(unsigned char data);
char readUSART(); //untested!!!!!
//int findAddress();  // this will find the hardware set Address
void selectADC(int p);
void DataOK();
void toggleLED();
void toggleBypass();


void interrupt isr(void) {
    if (RCIF) {
        while (RCIF) {
            addByte(readUSART());
            RCIF = 0;
        }
    }
}

int main(int argc, char** argv) {
    /*
     *  I/O
     */
    TRISBbits.TRISB7 = 0; //set RB7 as output TX
    TRISBbits.TRISB6 = 0; //set RB6 as output RS485 I/O
    TRISBbits.TRISB5 = 1; //set RB5 as input  RX
    TRISBbits.TRISB4 = 0; //set RB4 as output Interupt out
    TRISCbits.TRISC7 = 0; //set RC7 as output led
    TRISCbits.TRISC6 = 1; //set RC5 as input  SJ1
    TRISCbits.TRISC3 = 1; //set RC3 as input  SJ2
    TRISAbits.TRISA5 = 1; //set RA5 as input  SJ3
    TRISCbits.TRISC5 = 1; //set RC5 as input  SJ4
    TRISCbits.TRISC4 = 1; //set RC4 as input  SJ5
    TRISAbits.TRISA4 = 1; //set RA4 as input  SJ6
    TRISAbits.TRISA2 = 1; //set RA2 as input  Interupt in
    TRISCbits.TRISC2 = 0; //set RC2 as output Bypass
    TRISCbits.TRISC0 = 1; //set RC0 as input  Voltage
    TRISCbits.TRISC1 = 1; //set RC1 as input  NTC
    /*
     *  Analog pins that we want digital
     */
    ANSELCbits.ANSC2 = 0; //set RC2 as digital Bypass
    ANSELAbits.ANSA2 = 0; //set RA2 as digital Interupt in
    ANSELBbits.ANSB4 = 0; //set RB4 as digital Interupt out
    ANSELAbits.ANSA4 = 0; //set RA4 as digital SJ6             need to check this....
    ANSELCbits.ANSC3 = 0; //set RC3 as digital SJ2
    ANSELCbits.ANSC6 = 0; //set RC6 as digital SJ1
    ANSELCbits.ANSC7 = 0; //set RC7 as digital LED
    ANSELBbits.ANSB5 = 0; //set RB5 as digital RX
    /*
     *  UART
     */
    SPBRG = 12; // 38400 baud @ 4MHz = 12
    TXSTA = 0x24; // setup USART transmit
    CREN = 1;
    SYNC = 0;
    SPEN = 1;
    RCSTA = 0x90; // setup USART receive

    PIR1bits.RCIF = 0;
    PIE1bits.RCIE = 1;
    GIE = 1;
    PEIE = 1;
    /*
     *  ADC
     */
    FVRCONbits.ADFVR0 = 1; //vref 1.024  - 16mV accurcy
    FVRCONbits.ADFVR1 = 0;
    FVRCONbits.FVREN = 1;
    ADCON1bits.ADCS0 = 0; //Selecting the clk division factor = FOSC/4
    ADCON1bits.ADCS1 = 0;
    ADCON1bits.ADCS2 = 1;
    ADCON0bits.ADON = 1; //Turns on ADC module

    //address = findAddress(); // Find the hardware address...I hope!
    BYPASS =0;
    PORTBbits.RB6 = 1;
    PORTBbits.RB5 = 1;
    PORTBbits.RB7 = 1;

    //-----MAIN LOOP-------
    while (1) {

        checkAndRespond();
        DataOK();
        toggleLED();
        selectADC(0);
        selectADC(1);
        toggleBypass();

//        RS485_IO = 0;
//        __delay_ms(5);
//        putByteUSART(0xFF);
//
//        __delay_ms(5);
//        RS485_IO = 1;

    }

    return (EXIT_SUCCESS);
}

void selectADC(int p) {
    if (p == 0) {
        ADCON0bits.CHS0 = 1; //Selects channel x ( AN5 ) Vre set at 1.024
        ADCON0bits.CHS1 = 1;
        ADCON0bits.CHS2 = 1;
        ADCON0bits.CHS3 = 1;
        __delay_ms(10);
        ADCON0bits.GO_nDONE = 1;
        __delay_ms(10);
        if (ADCON0bits.GO_nDONE == 0) {
            ADC = ADRES;
            ADC = 78.046 * exp(-0.011 * ADC); //log(ADC);//(-0.318*ADC)+59.166;
            ADC += 2;
            volt = ADC;
            Data1=(int)volt;
            //putrsUSART("Volt");
        }
    }
    if (p == 1) {
        ADCON0bits.CHS0 = 1; //Selects channel 5 ( AN5 ) Temp
        ADCON0bits.CHS1 = 0;
        ADCON0bits.CHS2 = 1;
        ADCON0bits.CHS3 = 0;
        __delay_ms(10);
        ADCON0bits.GO_nDONE = 1;
        __delay_ms(10);
        if (ADCON0bits.GO_nDONE == 0) {
            ADC = ADRES;
            //ADC filtering?
            //putrsUSART("Temp");
//            Data2=ADC;
//            Data2=255-Data2;
            temp = (256 / ADC) - 1.0;
            temp = 1000.0 / temp;
            temp = temp / 1000.0; // (R/Ro)
            temp = log(temp); // ln(R/Ro)
            temp = temp / 3348.0; // 1/B * ln(R/Ro)
            temp = temp + 0.0033540164; // + (1/To)
            temp = 1.0 / temp; // Invert
            temp = temp - 273.15; // convert to C
            temp = temp*10;
            ADCON0bits.GO_nDONE = 1;
            Data2=(unsigned int) temp;
//            if(Data2>255){
//                Data2=255;
//            }
        }
    }
}

void putrsUSART(const char *data) {
    do {
        while (!(TXSTA & 0x02));
        TXREG = *data;
    } while (*data++);
}

void putrs1USART(const char *data) {
    do {
        while (!TXIF);
        TXREG = *data;
    } while (*data++);
}

void putByteUSART(unsigned char data) {


    while (!(TXSTA & 0x02));
    TXREG = data;

}

void putIntUSART(unsigned int data){
    putByteUSART((unsigned char)((data&0xFF00)>>8));
    putByteUSART((unsigned char)((data&0x00FF)>>0));

}

char readUSART() {
    char rx = 0;
    rx = RCREG;
    return rx;
}

void toggleLED() {
    if (laston > LEDtime) {
        LED = !LED;
//        BYPASS = !BYPASS;
        laston = 0;
    } else laston++;
}

void DataOK() {
    //Is data been here lately
    if (datahere == 1) {
        timecount = 0;
    } else {
        timecount += 1;
    }
    if (timecount >= Timeout) {
        LEDtime = LEDTCNO;
        timecount = Timeout;
    } else {
        LEDtime = LEDTCOK;
    }

}

void toggleBypass() {
    if ((volt >= VOLTBP) && (BPON == 0)) { //Turns on Bypass if over volt
        // Turn on Bypass
        BYPASS = 1;
        BPON = 1;
        Data3 = 0x0F;
    } else if ((BPON == 1) && (volt <= VOLTBPOF)) { //Turns off BP if voltage dropedd with BP on
        BYPASS = 0;
        BPON = 0;
        Data3 = 0x00;
    }
}

//int findAddress(){
//    int addr;
//    /*
//     * This is the order on the board:
//     * 1 - S1 - RC6 lsb
//     * 2 - S2 - RC3
//     * 3 - S5 - RC4
//     * 4 - S4 - RC5
//     * 5 - S6 - RA4
//     * 6 - S3 - RA5 msb
//     */
//    if(PORTAbits.RA5 == 1){ //1xxxxx
//        if(PORTAbits.RA4 == 1){ //11xxxx
//            if(PORTCbits.RC5 == 1){ //111xxx
//                if(PORTCbits.RC4 == 1){ //1111xx
//                    if(PORTCbits.RC3 == 1){ //11111x
//                        if(PORTCbits.RC6 == 1){ //111111
//                            //111111 =
//                            addr = 0xff;
//                        }
//                        else{
//                            //111110
//                            addr = 0xff;
//                        }
//                    }
//                    else{ //11110x
//                        if(PORTCbits.RC6 == 1){
//                            //111101
//                            addr = 0xff;
//                        }
//                        else{
//                            //111100
//                            addr = 0xff;
//                        }
//                    }
//                }
//                else{ //1110xx
//                        if(PORTCbits.RC6 == 1){ //11101x
//                            if(PORTCbits.RC6 == 1){
//                                //111011
//                                addr = 0xff;
//                            }
//                            else{ //111010
//                                addr = 0xff;
//                            }
//                        }
//                        else{ //11100x
//                            if(PORTCbits.RC6 == 1){ //111001
//                               addr = 0xff;
//                            }
//                            else{ //111000
//                            addr = 0xff;
//                            }
//                        }
//                }
//             }
//            else{
//
//            }
//       }
//        else{
//
//        }
//    }
//    else{
//        if(PORTCbits.RC3 == 1){
//
//        }
//        else{
//
//        }
//    }
//    return addr;
//}
void addByte(unsigned char incomming) {
    if (!callByte) { //if you havent heard a start byte yet
        if (incomming == startByte) //and the incomming is the start byte
        {
            callByte = 1; //set flag
            return; //bounce
        } else return; //if its not start byte and we havent heard one ignor it

    } else //else we have had our start byte
    {
        if (!me) //if we havent heard if its us after start byte
        {
            if (incomming == address) // if it is us
                me = 1; //set flag
            else //else
            {
                callByte = 0;
                return;
            }
        } else {
            if ((incomming == stopByte) && stop1) { //if this is second stop byte in a row terminate packet read
                heard = 1;
                callByte = 0;
                stop1 = 0;
                me = 0;
                return;
            } else if (incomming == stopByte) //else if this is a stopByte w/o heard one yet
            {
                stop1 = 1;
                return;
            } else {
                switch(incomming){
                    case 0xBB:
                        BYPASS=1;
                    break;
                    case 0xBA:
                        BYPASS=0;
                        break;
                }


            }
        }
    }
}

void checkAndRespond() {
    //Serial Analize here?
    datahere = 0;
    if (heard != 0) {
        RS485_IO = 0;
        datahere = 1;
        __delay_ms(5);
        putByteUSART(0xEC);
        putByteUSART(address);
        putByteUSART(Data1); //Voltage Temp
        putByteUSART((unsigned char)Data2); //Temp
        putByteUSART(Data3);
        putByteUSART(0x99);
        putByteUSART(0x99);
        __delay_ms(5);
        LED = 1;
        RS485_IO = 1;
        counter = 0;
        callByte = 0;
        me = 0;
        heard = 0;
    }
}