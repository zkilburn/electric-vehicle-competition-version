
void initSystem(){  
  pinMode(Relay,OUTPUT);
  digitalWrite(Relay,LOW);
  writeEEPROM(0,1);
  writeEEPROM(0,2);
  writeEEPROM(0,3);
  writeEEPROM(0,4);
  
  pinMode(RS485_direction,OUTPUT);
  pinMode(haltPin,OUTPUT);
  pinMode(LED,OUTPUT);
  pinMode(WDT,OUTPUT);
  
  pinMode(Addr1,OUTPUT);  
  pinMode(Addr2,OUTPUT);  
  pinMode(Addr3,OUTPUT);  
  pinMode(Addr4,OUTPUT);
  
  pinMode(RightRedLED,INPUT);
  pinMode(RightGreenLED,INPUT);
  pinMode(LeftGreenLED,INPUT);
  pinMode(LeftRedLED,INPUT);
  
  pinMode(DashGreen,OUTPUT);
  pinMode(DashRed,OUTPUT);
  
  digitalWrite(Addr1,HIGH);
  digitalWrite(Addr2,HIGH);
  digitalWrite(Addr3,HIGH);
  digitalWrite(Addr4,HIGH);
  
  digitalWrite(RS485_direction,LOW);
 
  digitalWrite(LED,LOW);
  
  digitalWrite(DashGreen,LOW);
  digitalWrite(DashRed,LOW);
  
  Serial.begin(38400);
  Wire.begin();
  //initADC();
   writeDAC(0,1);
  writeDAC(0,2);
  writeDAC(0,3);
  writeDAC(0,4);
}


