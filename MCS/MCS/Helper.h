//Helper methods to make routines that can be used anywhere anytime


//power control relay for motor control system (set HIGH car stops)
void toggleRelay(){
  if (relayState)
    digitalWrite(Relay,LOW);
  else
    digitalWrite(Relay,HIGH);
  relayState=!relayState; 
}
//toggle the LED
void toggleLED(){
  if (LEDState)
    digitalWrite(LED,LOW);
  else
    digitalWrite(LED,HIGH);
  LEDState=!LEDState; 
}


void indicateThrottleLED(){
    throttleIndicator++;
  if(throttleIndicator>throttle1){
    throttleIndicator=0;
    toggleLED(); 
  }
}
void indicateADC_LED(){
  digitalWrite(LED,HIGH);
  delay(readADC(true,false));
  digitalWrite(LED,LOW);  
  delay(readADC(true,false));
  
}
void stopCar(){
 if(!relayState){
    digitalWrite(Relay,HIGH);    
    relayState=1;    
    
    //turn off the DAC (set zeros)
    shutdownOutput();
   } 
}
