//Structure for DAC communication and a demonstration write string


void shutdownOutput(){
 writeDAC(0,1); 
 writeDAC(0,2); 
 writeDAC(0,3); 
 writeDAC(0,4); 
}
void writeEEPROM(int voltage,int channel)
{
  
  switch(channel){
  case t1:    
      digitalWrite(Addr1,LOW);
    break;
  case t2:
      digitalWrite(Addr2,LOW);  
    break;
  case b1:
      digitalWrite(Addr3,LOW);  
    break;
  case b2:
      digitalWrite(Addr4,LOW);  
    break;
  }
  delayMicroseconds(20);
  float sendV=voltage;
  sendV=sendV;//((sendV/100)*4095);
  lowb =(((int)sendV & 0x000F)<<4);
  highb=(((int)sendV & 0x0FF0)<<4);
  Wire.beginTransmission(0x61); // transmit to DAC  
  Wire.write(0x60);
  Wire.write(highb);            // sends    (high byte)  xxxx xxxx
  Wire.write(lowb);              // sends    (low byte) xxxx 0000
  Wire.endTransmission();      // stop transmitting
  
  delayMicroseconds(20);
  switch(channel){
  case t1:    
      digitalWrite(Addr1,HIGH);
    break;
  case t2:
      digitalWrite(Addr2,HIGH);  
    break;
  case b1:
      digitalWrite(Addr3,HIGH);  
    break;
  case b2:
      digitalWrite(Addr4,HIGH);  
    break;
  }
}

void writeDAC(int voltage, int channel){
  switch(channel){
  case t1:    
      digitalWrite(Addr1,LOW);
    break;
  case t2:
      digitalWrite(Addr2,LOW);  
    break;
  case b1:
      digitalWrite(Addr3,LOW);  
    break;
  case b2:
      digitalWrite(Addr4,LOW);  
    break;
  }
  delayMicroseconds(20);
  float sendV=voltage;
  sendV=sendV;//((sendV/100)*4095);
  lowb =((int)sendV & 0x00FF);
  highb=(((int)sendV & 0xFF00)>>8);
  Wire.beginTransmission(0x61); // transmit to DAC  
  Wire.write(highb);            // sends    (high byte)  xxxx xxxx
  Wire.write(lowb);              // sends    (low byte) xxxx 0000
  Wire.endTransmission();      // stop transmitting
  
  delayMicroseconds(20);
  switch(channel){
  case t1:    
      digitalWrite(Addr1,HIGH);
    break;
  case t2:
      digitalWrite(Addr2,HIGH);  
    break;
  case b1:
      digitalWrite(Addr3,HIGH);  
    break;
  case b2:
      digitalWrite(Addr4,HIGH);  
    break;

  }
}


